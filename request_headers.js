const http = require("http");
const fs = require('fs');
const fsPromises = fs.promises;
const path = require('path');

function writeLine(res, text) {
    res.write(`<span>${text}</span><br />`);
}

function writeHeader(res, text) {
    res.write(`<h1>${text}</h1>`);
}

const server = http.createServer(function (req, res) {
    const url = req.url;
  
    res.writeHead(200, { "Content-Type": "text/html" });
    
    writeHeader(res, "Request Headers");
    writeLine(res, "req.url = " + req.url);
    for (let p in req.headers) {
        writeLine(res, "req.headers." + p + " = " + req.headers[p] + "");
    }
    
    res.end();
});

server.listen(8090);