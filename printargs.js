// Finds a command line argument that is a JSON expression.
// Example invocation:
//   node printargs.js '{"port":1234,"configfile":"/etc/foo.json"}'
process.argv.slice(2).forEach(function (val, index, array) {
    console.log(index + ': ' + val)
    if (val.startsWith('{') && val.endsWith('}')) {
        const settings = JSON.parse(val)
        console.log(settings.port)
        console.log(settings.configfile)
    }
})