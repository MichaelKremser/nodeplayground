const http = require("http");
const fs = require('fs');
const path = require('path');

function writeHeader(res, text) {
    res.write(`<h1>${text}</h1>`);
}

function getFileContentFromAnyDirectory(filename, directoriesToTry, log) {
    var filepath;
    var content;
    var result;
    for (let directoryToTry of directoriesToTry) {
        filepath = path.join(directoryToTry, filename);
        log("trying " + filepath);
        try {
            content = fs.readFileSync(filepath);
            result = true;
        }
        catch {
            result = false;
        }
        if (result) {
            log("found " + filepath);
            return content;
        }
    }
    return "";
}

const server = http.createServer(function (req, res) {
    res.writeHead(200, { "Content-Type": "text/html" });

    writeHeader(res, "Find File");
    const directoriesToTry = [ "/etc", __dirname, "/root" ];

    var content = getFileContentFromAnyDirectory('README.md', directoriesToTry, console.log);
    res.write(`<pre>${content}</pre>`);
    
    res.end();
});

server.listen(8091);